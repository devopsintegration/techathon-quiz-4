
public class Calculate {

    public static void main(String[] args) {
        int sum = 0;
        for(int i = 0;i < 100000;i++) {
            if(i % 5 == 0 || i % 7 == 0) {
                sum += i;
            }
        }

        System.out.println(sum);
        
    }
}