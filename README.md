# Techathon Quiz 4
## Question
If we list all the natural numbers below 20 that are multiples of 5 or 7, we get 5, 7, 10, 14, and 15. The sum of these multiples is 51.

Find the sum of all the multiples of 5 or 7 below 100,000

## Answer
Open the directory with the language of preference to find the code to solve the question in that language.

Languages available;
- Java
- Python
- PHP



